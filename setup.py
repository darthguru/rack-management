from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in rack_management/__init__.py
from rack_management import __version__ as version

setup(
	name='rack_management',
	version=version,
	description='an app to add the rack system over the existing waresouse system',
	author='Dhupar Group',
	author_email='gurshish@dhupargroup.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
