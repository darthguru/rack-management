from frappe import _

def get_data():
	return [
		{
			"module_name": "Rack Management",
			"color": "blue",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Rack Management")
		}
	]
