// Copyright (c) 2021, Dhupar Group and contributors
// For license information, please see license.txt

frappe.ui.form.on('Rack Picklist', {

	setup: function(frm) {
		frm.set_query('source_rack', 'locations', () => {
			return {
				filters: {
					'parent_warehouse': ['=', frm.doc.parent_warehouse]
				}
			};
		});
		frm.set_query('destination_rack', 'locations', () => {
			var filt = {}
			if (frm.doc.purpose == "Delivery"){
					filt = {
						filters: {
							'parent_warehouse': ['=', frm.doc.parent_warehouse],
							'is_primary': 1
						}
					}
			}
			else{
				filt = {
					filters: {
						'parent_warehouse': ['=', frm.doc.parent_warehouse]
					}
				}
			}
			return filt ;
		});
		frm.set_query('item_code', 'locations', () => {
			return erpnext.queries.item({ "is_stock_item": 1 });
		});
	}

});

frappe.ui.form.on('Rack Picklist Item', {

	item_code: (frm, cdt, cdn) => { 
		let row = frappe.get_doc(cdt, cdn);
		var item_code = row.item_code;
		var source_rack = row.source_rack;
		get_stock(row.item_code, row.source_rack).then(data => {
			frappe.model.set_value(cdt, cdn, 'stock_qty', data.qty);
		});
		
	},
	source_rack: (frm, cdt, cdn) => { 
		let row = frappe.get_doc(cdt, cdn);
		var item_code = row.item_code;
		var source_rack = row.source_rack;
		get_stock(row.item_code, row.source_rack).then(data => {
			frappe.model.set_value(cdt, cdn, 'stock_qty', data.qty);
		});
	}

});

function get_stock(item_code, source_rack=null){
	if(item_code){
		return frappe.xcall('rack_management.rack_management.doctype.rack_picklist.rack_picklist.get_stock', {
			item_code,
			source_rack
		});
	}
}