# Copyright (c) 2021, Dhupar Group and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.utils import cstr, flt, getdate, cint, nowdate, add_days, get_link_to_form, strip_html
from sqlparse import filters

class RackPicklist(Document):
	def validate(self):
		self.validate_stock()

	def validate_stock(self):
		# check if there is enough stock to be transfered
		for i in self.locations:
			stock = get_stock(i.item_code, i.source_rack)
			if not stock:
				frappe.throw("No stock for item " + i.item_code + " on rack " + i.source_rack)
			if i.qty and i.qty > stock.qty:
				frappe.throw("Not enough stock for item " + i.item_code + " on rack " + i.source_rack)

	def on_submit(self):
		self.transfer_material()

	def transfer_material(self):
		# transfer the material between racks
		for item in self.locations:
			
			deduct_stock_from_rack(item.item_code, item.source_rack, item.qty)

			add_stock_to_rack(item.item_code, item.destination_rack, item.qty)

	def set_item_locations(self):

		if self.purpose == "Delivery":
			
			primary_rack = get_primary_rack(self.parent_warehouse)

			for i in self.locations:
				total_qty = i.qty
				i.destination_rack = primary_rack.name
				all_racks = frappe.get_list("Racking Warehouse",fields=["name"],filters={'parent_warehouse':self.parent_warehouse})
				rack_list = frappe.get_list('Rack Stock',fields=["parent","item_code","qty"],filters={"item_code":i.item_code,"parent":{"in",str(all_racks)}})
				current_updated = False
				for rack_item in rack_list:
					last_flag = False
					if total_qty <= rack_item.qty and not current_updated:
						i.source_rack = rack_item.parent
						total_qty = 0
					
					if total_qty < rack_item.qty and total_qty > 0 and current_updated:
						row = self.append('locations',{})
						row.item_code = i.item_code
						row.source_rack = rack_item.parent
						row.qty = total_qty

						total_qty = total_qty - rack_item.qty

					if total_qty > rack_item.qty:
						if not current_updated:
							i.source_rack = rack_item.parent
							i.qty = rack_item.qty
							current_updated = True
						
						else:
							row = self.append('locations',{})
							row.item_code = i.item_code
							row.source_rack = rack_item.parent
							row.qty = rack_item.qty

						total_qty = total_qty - rack_item.qty

				if total_qty > 0:
					frappe.msgprint("Need " + str(total_qty) + " Nos of item " + i.item_code + " in " + self.parent_warehouse)
			
			
@frappe.whitelist()
def get_stock(item_code, source_rack = None):
	# get stock for an item on individual rack
	if(source_rack):
		details = frappe.db.get_value('Rack Stock', {'item_code': item_code, 'parent': source_rack}, ['name', 'item_code', 'qty'], as_dict=1)

		return details



def deduct_stock_from_rack(item_code, source_rack, qty):
	# deduct stock from the target rack
	source_stock_locations = get_stock(item_code, source_rack)

	# remove source rack if total qty
	if source_stock_locations.qty == qty:
		frappe.db.delete("Rack Stock",{ 'name': source_stock_locations.name })
			
	else:
		frappe.db.set_value('Rack Stock', source_stock_locations.name, 'qty', source_stock_locations.qty - qty, update_modified = False)



def add_stock_to_rack(item_code, destination_rack, qty):
	# add stock to target rack
	destination_stock_locations = get_stock(item_code, destination_rack)

	# add new row at destination if no current stock
	if destination_stock_locations:
		frappe.db.set_value('Rack Stock', destination_stock_locations.name, 'qty', destination_stock_locations.qty + qty, update_modified = False)

	else:
		destination_stock_locations = frappe.get_doc("Racking Warehouse", destination_rack)
		destination_stock_locations.append('rack_stock',{
			'item_code': item_code,
			'qty': qty
		})
		destination_stock_locations.save()
		frappe.db.commit()



def get_primary_rack(warehouse):

	return frappe.db.get_value('Racking Warehouse',{'parent_warehouse': warehouse, 'is_primary':1	}, ['name'], as_dict=1)


@frappe.whitelist()
def create_picklist_so(source_name, target_doc=None):
	def update_item_quantity(source, target, source_parent):
		target.qty = flt(source.qty) - flt(source.delivered_qty)
		# target.stock_qty = (flt(source.qty) - flt(source.delivered_qty)) * flt(source.conversion_factor)

	doc = get_mapped_doc('Sales Order', source_name, {
		'Sales Order': {
			'doctype': 'Rack Picklist',
			'validation': {
				'docstatus': ['=', 1]
			},
			'field_map':{
				'set_warehouse': 'parent_warehouse'
			}
		},
		'Sales Order Item': {
			'doctype': 'Rack Picklist Item',
			'field_map': {
				'item_code': 'item_code',
				'qty': 'qty'
			},
			'postprocess': update_item_quantity,
			'condition': lambda doc: abs(doc.delivered_qty) < abs(doc.qty) and doc.delivered_by_supplier!=1
		},
	}, target_doc)

	doc.purpose = 'Delivery'

	doc.set_item_locations()

	return doc


@frappe.whitelist()
def create_picklist_dn(source_name, target_doc=None):

	doc = get_mapped_doc('Delivery Note', source_name, {
		'Delivery Note': {
			'doctype': 'Rack Picklist',
			'validation': {
				'docstatus': ['=', 0]
			},
			'field_map':{
				'set_warehouse': 'parent_warehouse'
			}
		},
		'Delivery Note Item': {
			'doctype': 'Rack Picklist Item',
			'field_map': {
				'item_code': 'item_code',
				'qty': 'qty'
			}			
		},
	}, target_doc)

	doc.purpose = 'Delivery'

	doc.set_item_locations()

	return doc