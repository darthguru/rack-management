# Copyright (c) 2021, Dhupar Group and contributors
# For license information, please see license.txt

# import frappe
from __future__ import unicode_literals
from frappe.model.document import Document
import frappe
from frappe.utils import cint, cstr, nowdate
from frappe import throw, _
from frappe.utils.nestedset import NestedSet, get_ancestors_of, get_descendants_of
from rack_management.rack_management.doctype.rack_picklist.rack_picklist import get_stock, add_stock_to_rack, deduct_stock_from_rack, get_primary_rack
from erpnext.stock.utils import get_latest_stock_qty, get_stock_balance

class RackingWarehouse(Document):
	
	def before_insert(self):

		# Check if there is a Primary Rack for the warehouse before adding		
		racks = frappe.db.sql("select is_primary,name from `tabRacking Warehouse` where parent_warehouse = %s",
			self.parent_warehouse, as_dict=1)

		pr_flg = False
		
		for d in racks:
			if d.is_primary ==1:
				pr_flg = True
				
		if self.is_primary == 1 and pr_flg == True:
			frappe.throw("Primary rack for " + self.parent_warehouse + " already exists")
		
		if self.is_primary !=1 and pr_flg == False:
			frappe.throw("Add Primary rack for " + self.parent_warehouse + " First")

		###

		# Add items from parent warehouse for primary rack
		if self.is_primary:
			item_list = frappe.db.sql("select * from `tabBin` where warehouse = %s",
				self.parent_warehouse, as_dict=1)

			for i in item_list:
				if i.actual_qty>0:
					row = self.append('rack_stock',{})
					row.item_code = i.item_code
					row.qty = i.actual_qty



# extensions to catch defferent stock transactions for warehouses
def on_dn_submit(doc,method = None):
	racking_warehouses = frappe.get_all(
		"Racking Warehouse",
		fields=["parent_warehouse"],
		filters={
			"parent_warehouse": ("is", "set"),
			"is_primary": 1
		},
		pluck="parent_warehouse")

	# check if adiquate stock is picked
	for i in doc.items:
<<<<<<< HEAD
		is_racking_warehouse = None
		is_racking_warehouse = get_primary_rack(i.warehouse)
		if is_racking_warehouse:
=======
		if i.warehouse in racking_warehouses:
>>>>>>> 0298ff146f751c5015141cec45c7b34a0dbdd79c
			current_stock = get_stock(i.item_code, is_racking_warehouse.name)
			if not current_stock:
				frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)
			if current_stock.qty < i.qty:
				frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)

	# update items on rack
	for i in doc.items:
<<<<<<< HEAD
		is_racking_warehouse = get_primary_rack(i.warehouse)
		if is_racking_warehouse:
=======
		if i.warehouse in racking_warehouses:
>>>>>>> 0298ff146f751c5015141cec45c7b34a0dbdd79c
			deduct_stock_from_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_dn_cancel(doc,method = None):

	# update items on rack
	for i in doc.items:
		is_racking_warehouse = get_primary_rack(i.warehouse)
		if is_racking_warehouse:
			add_stock_to_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_si_submit(doc,method = None):

	# check if adiquate stock is picked
	if doc.update_stock ==1:
		for i in doc.items:
			is_racking_warehouse = None
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:
				current_stock = get_stock(i.item_code, is_racking_warehouse.name)
				if not current_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)
				if current_stock.qty < i.qty:
					frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)

		# update items on rack
		for i in doc.items:
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:
				deduct_stock_from_rack(i.item_code, is_racking_warehouse.name, i.qty)

def on_si_cancel(doc,method = None):

	# check if adiquate stock is picked
	if doc.update_stock ==1:
		# update items on rack
		for i in doc.items:
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:
				add_stock_to_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_pi_submit(doc,method = None):

	# check if update stock is ticked and warehouse is racking warehouse
	if doc.update_stock ==1:
		for i in doc.items:
			is_racking_warehouse = None
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:	
				add_stock_to_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_pi_cancel(doc,method = None):

	# check if update stock is ticked and warehouse is racking warehouse
	if doc.update_stock ==1:
		for i in doc.items:
			is_racking_warehouse = None
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:
				current_stock = get_stock(i.item_code, is_racking_warehouse.name)
				if not current_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)
				if current_stock.qty < i.qty:
					frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)

		for i in doc.items:
			is_racking_warehouse = None
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:	
				deduct_stock_from_rack(i.item_code, is_racking_warehouse.name, i.qty)

def on_pr_submit(doc,method = None):

	# check if update stock is ticked and warehouse is racking warehouse
	for i in doc.items:
		is_racking_warehouse = None
		is_racking_warehouse = get_primary_rack(i.warehouse)
		if is_racking_warehouse:			
			add_stock_to_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_pr_cancel(doc,method = None):

	for i in doc.items:
			is_racking_warehouse = None
			is_racking_warehouse = get_primary_rack(i.warehouse)
			if is_racking_warehouse:
				current_stock = get_stock(i.item_code, is_racking_warehouse.name)
				if not current_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)
				if current_stock.qty < i.qty:
					frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_racking_warehouse.name)

	# check if update stock is ticked and warehouse is racking warehouse
	for i in doc.items:
		is_racking_warehouse = None
		is_racking_warehouse = get_primary_rack(i.warehouse)
		if is_racking_warehouse:			
			deduct_stock_from_rack(i.item_code, is_racking_warehouse.name, i.qty)


def on_ste_submit(doc, method = None):

	is_rack2 = None

	# Validate racks
	for i in doc.items:
		is_rack1 = None
		is_rack1 = get_primary_rack(i.s_warehouse)
		if is_rack1:
				current_stock = get_stock(i.item_code, is_rack1.name)
				if not current_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_rack1.name)

				if current_stock.qty < i.qty:
					frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_rack1.name)

	for i in doc.items:
		is_rack2 = get_primary_rack(i.t_warehouse)
		is_rack1 = get_primary_rack(i.s_warehouse)
		if is_rack2:			
			add_stock_to_rack(i.item_code, is_rack2.name, i.qty)

		if is_rack1:
			deduct_stock_from_rack(i.item_code, is_rack1.name, i.qty)
	
def on_ste_cancel(doc, method = None):

	is_rack2 = None

	# Validate racks
	for i in doc.items:
		is_rack1 = None
		is_rack1 = get_primary_rack(i.t_warehouse)
		if is_rack1:
				current_stock = get_stock(i.item_code, is_rack1.name)
				if not current_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " to " + is_rack1.name)

				if current_stock.qty < i.qty:
					frappe.throw("Please transfer " + str(i.qty-current_stock.qty) + " Nos of " + i.item_code + " to " + is_rack1.name)

	for i in doc.items:
		is_rack2 = get_primary_rack(i.s_warehouse)
		is_rack1 = get_primary_rack(i.t_warehouse)
		if is_rack2:			
			add_stock_to_rack(i.item_code, is_rack2.name, i.qty)

		if is_rack1:
			deduct_stock_from_rack(i.item_code, is_rack1.name, i.qty)


def on_sr_submit(doc, method = None):
	
	if doc._action=="submit":
			
		for i in doc.items:
			p_rack = get_primary_rack(i.warehouse)
			if p_rack:
				current_stock = get_latest_stock_qty(i.item_code,i.warehouse)
				r_stock = get_stock(i.item_code,p_rack)
				if current_stock < i.qty:
					add_stock_to_rack(i.item_code, p_rack, (i.qty-current_stock))
					continue

				if not r_stock:
					frappe.throw("Please transfer " + str(i.qty) + " Nos of " + i.item_code + " rack " + p_rack.name + " to complete this transaction")

				if current_stock > i.qty:
					if r_stock < i.qty:
						frappe.throw("Please transfer " + str(i.qty-r_stock) + " to rack " + p_rack + " to complete this transaction")
					deduct_stock_from_rack(i.item_code, p_rack, (r_stock-i.qty))


				
