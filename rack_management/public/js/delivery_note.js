

frappe.ui.form.on("Delivery Note", {

    refresh: function(frm) {

        if(cur_frm.doc.docstatus == 0){
            setTimeout(() => {
                // cur_frm.page.remove_inner_button(__('Installation Note'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Quality Inspection(s)'),  __('Create'));
                cur_frm.add_custom_button(__('Pick List'), () => create_rack_pick_list(frm), __('Create'));
            },200);
        }
    }

});


function create_rack_pick_list(frm){

    frappe.model.open_mapped_doc({
        method: "rack_management.rack_management.doctype.rack_picklist.rack_picklist.create_picklist_dn",
        frm: frm
    })

}