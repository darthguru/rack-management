
frappe.ui.form.on("Sales Order", {

    refresh: function(frm) {

        if(cur_frm.doc.docstatus == 1){
            setTimeout(() => {
                cur_frm.page.remove_inner_button(__('Pick List'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Work Order'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Request for Raw Materials'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Project'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Payment Request'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Payment'),  __('Create'));
                cur_frm.page.remove_inner_button(__('Subscription'),  __('Create'));
                cur_frm.add_custom_button(__('Pick List'), () => create_rack_pick_list(frm), __('Create'));
            },100);
        }
        // cur_frm.add_custom_button(__('Pick List'), () => this.create_rack_pick_list(), __('Create'));

    }

});

function create_rack_pick_list(frm){

    frappe.model.open_mapped_doc({
        method: "rack_management.rack_management.doctype.rack_picklist.rack_picklist.create_picklist_so",
        frm: frm
    })

}